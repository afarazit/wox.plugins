﻿# Change Log
All notable changes to this project will be documented in this file.
 
The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).
 
## [Unreleased]
 
### Added
- Configurable paths via Settings menu
 
## [1.0.1] - 2021-01-07
 
### Added
- Default action keyword 's'

### Fixed
- Fixed author name type 

 
## [1.0.0] - 2021-01-07
 
First version
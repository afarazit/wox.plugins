﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using RestSharp;
using Wox.Plugin;

namespace Afarazit.Wox.Plugin.Notes
{
    public class Main : IPlugin
    {
        public List<string> Titles { get; set; } = new List<string>();
        public List<string> Labels { get; set; } = new List<string>();
        private string _endpoint = "http://notes.afarazit.eu/api";
        private string _apiKey = "5Cu8NZIZQtdrUpo4igbA2JdcoFtVyP0y4";

        private int _titlesCachedMinute = -1;
        private int _labelsCachedMinute = -1;

        public void Init(PluginInitContext context)
        {

        }

        public List<Result> Query(Query query)
        {
            GetTitles();
            GetLabels();

            string note = string.Join(" ", query.Terms);

            var body = "";
            var title = "";
            var label = "";
            var titleTag = "t:";
            var labelTag = "l:";
            bool hasTitle = false;
            bool hasLabel = false;
            bool parsed = false;

            Match match;
            if (note.Contains(titleTag) && note.Contains(labelTag))
            {
                // body
                match = Regex.Match(note, ".*(?=t:)");
                body = match.Value.Trim();

                // title
                match = Regex.Match(note, "t:.*(?=l:)");
                title = match.Value.Replace(titleTag, "").Trim();

                // label
                match = Regex.Match(note, "l:.*");
                label = match.Value.Replace(labelTag, "").Trim();

                parsed = true;
                hasTitle = true;
                hasLabel = true;
            }

            if (note.Contains(titleTag) && !parsed)
            {
                // body
                match = Regex.Match(note, ".*(?=t:)");
                body = match.Value.Trim();

                // title
                match = Regex.Match(note, "t:.*");
                title = match.Value.Replace(titleTag, "").Trim();

                parsed = true;
                hasTitle = true;
            }

            if (note.Contains(labelTag) && !parsed)
            {
                // body
                match = Regex.Match(note, ".*(?=l:)");
                body = match.Value.Trim();

                // label
                match = Regex.Match(note, "l:.*");
                label = match.Value.Replace(labelTag, "").Trim();

                parsed = true;
                hasLabel = true;
            }

            if (!note.Contains(titleTag) && !note.Contains(labelTag) && !parsed)
            {
                // body
                body = note.Trim();
            }

            body = body.TrimStart(query.ActionKeyword.ToCharArray()).Trim();

            string resultTitle = body != "" ? body : "New note!";
            string resultSubtitle = "";
            if (hasTitle)
            {
                bool titleExists = Titles.FirstOrDefault(t => t == title) != null;
                string titleStatus = title + (!titleExists ? " (new)" : " (update)");
                resultSubtitle += $"Title: {titleStatus}";
            }
            else
            {
                resultSubtitle += "Without Title";
            }

            if (hasLabel)
            {
                bool labelExists = Labels.FirstOrDefault(l => l == label) != null;
                string labelStatus = label + (!labelExists ? " (new)" : "(exists)");

                resultSubtitle += $" - Label: {labelStatus}";
            }
            else
            {
                resultSubtitle += " - Without Label";
            }

            var list = new List<Result>();

            list.Add(new Result
            {
                Title = resultTitle,
                SubTitle = resultSubtitle,
                IcoPath = "noteIcon.jpg",
                Action = arg => { 
                    InvalidateCache();
                    return Send(body, title, label);
                }
            });
            return list;
        }

        private bool Send(string body, string title, string label)
        {
            var client = new RestClient($"{_endpoint}/AddNote");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AlwaysMultipartFormData = true;
            request.AddParameter("ApiKey", _apiKey);
            request.AddParameter("Body", body);
            request.AddParameter("Title", title);
            request.AddParameter("Label", label);
            IRestResponse response = client.Execute(request);
            string content = JsonConvert.DeserializeObject<string>(response.Content);
            return content == "SAVED";
        }

        private void GetTitles()
        {
            bool useCache = _titlesCachedMinute == DateTime.Now.Minute;
            if (Titles.Count != 0 && !useCache)
            {
                return;
            }

            var client = new RestClient($"{_endpoint}/GetTitles?ApiKey={_apiKey}");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            IRestResponse response = client.Execute(request);
            Titles = JsonConvert.DeserializeObject<List<string>>(response.Content);
            _titlesCachedMinute = DateTime.Now.Minute;
        }

        private void GetLabels()
        {
            bool useCache = _labelsCachedMinute == DateTime.Now.Minute;
            if (Labels.Count != 0 && !useCache)
            {
                return;
            }

            var client = new RestClient($"{_endpoint}/GetLabels?ApiKey={_apiKey}");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            IRestResponse response = client.Execute(request);
            Labels = JsonConvert.DeserializeObject<List<string>>(response.Content);
            _labelsCachedMinute = DateTime.Now.Minute;
        }

        private void InvalidateCache()
        {
            _titlesCachedMinute = -1;
            _labelsCachedMinute = -1;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using FuzzySharp;
using FuzzySharp.Extractor;
using FuzzySharp.SimilarityRatio.Scorer.Composite;
using FuzzySharp.SimilarityRatio.Scorer.StrategySensitive;
using Wox.Plugin;

namespace Afarazit.WoxPlugin.VsSln
{
    public class Main : IPlugin
    {
        protected List<KeyValuePair<string, string>> SlnFiles = new List<KeyValuePair<string, string>>();
        private List<string> DirectoriesToScan { get; set; }
        private List<string> IgnoredDirectories { get; set; }

        public void Init(PluginInitContext context)
        {
            Task.Run(() =>
            {
                DirectoriesToScan = new List<string>()
                {
                    @"C:\Projects"
                };

                IgnoredDirectories = new List<string>()
                {
                    @"C:\Projects\mangos\ACE-6.5.8"
                };

                foreach (string directory in DirectoriesToScan)
                {
                    var pathsInDirectory =
                        Directory.GetFiles(directory, "*.sln", SearchOption.AllDirectories);

                    foreach (string path in pathsInDirectory)
                    {
                        var skipPath = false;
                        foreach (var ignoredDir in IgnoredDirectories.Where(ignoredDir => path.StartsWith(ignoredDir)))
                        {
                            skipPath = true;
                        }

                        if (skipPath)
                        {
                            continue;
                        }

                        SlnFiles.Add(new KeyValuePair<string, string>(Path.GetFileName(path), path));
                    }
                }
            });
        }

        public List<Result> Query(Query query)
        {
            string term = query.ActionKeyword != "*" ? query.Terms[1] : query.Terms[0];
            List<KeyValuePair<string, string>> foundPaths = SlnFiles.Where(a => a.Key.ToLowerInvariant().Contains(term.ToLowerInvariant())).ToList();
            // var fuzzy = Process.ExtractAll(term.ToLowerInvariant(), SlnFiles.Select(a => a.Key),null, new TokenAbbreviationScorer(), cutoff: 50);
            // List<string> matches = fuzzy.Select(t => t.Value).ToList();
            // List<KeyValuePair<string, string>> foundPaths = SlnFiles.Where(sln => matches.Contains(sln.Key)).ToList();

            return foundPaths.Select(pair => new Result
                {
                    Title = pair.Key,
                    SubTitle = pair.Value,
                    IcoPath = "vsIcon.png",
                    Action = arg =>
                    {
                        System.Diagnostics.Process.Start(pair.Value);
                        return true;
                    }
                })
                .ToList();
        }

    }
}
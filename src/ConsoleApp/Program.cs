﻿using Afarazit.WoxPlugin.VsSln;
using Wox.Plugin;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Notes();
        }

        protected static void Notes()
        {
            var main = new Main();
            main.Init(new PluginInitContext());
            // var query = new Query() { ActionKeyword = "n", Terms = new[] { "agora", "afterburner", "gia", "c0smo", "kai", "atnonis", "moros", "loros", "voros", "t:shopping", "list", "l:eve", "label" } };
            // var query = new Query() { ActionKeyword = "n", Terms = new[] { "agora", "afterburner", "gia", "c0smo", "kai", "atnonis", "moros", "loros", "voros", "l:eve", "label" } };
            // var query = new Query() { ActionKeyword = "n", Terms = new[] { "agora", "afterburner", "gia", "c0smo", "kai", "atnonis", "moros", "loros", "voros", "t:shopping", "list"  } };
            // var query = new Query() { ActionKeyword = "n", Terms = new[] { "agora", "afterburner", "gia", "c0smo", "kai", "atnonis", "moros", "loros", "voros"  } };
            var query = new Query() { ActionKeyword = "s", Terms = new[] { "*", "afarC" } };

            main.Query(query);
        }
    }
}

using Afarazit.WoxPlugin.VsSln;
using Wox.Plugin;
using Xunit;

namespace Afarazit.Wox.Plugin.VsSln.Tests
{
    public class WoxSln
    {
        [Fact]
        public void Query_Return_Results()
        {
            var main = new Main();
            main.Init(new PluginInitContext());
            var query = new Query() {ActionKeyword = "sln", Terms = new[] {"Afarazit", "Notifie"}};
            main.Query(query);
        }
    }
}